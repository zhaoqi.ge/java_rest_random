#!/bin/sh

curl https://start.spring.io/starter.tgz \
    -d dependencies=web \
    -d description="Demo rest Random with Spring Boot" \
    -d groupId=if4030.rest \
    -d packageName=if4030.rest \
    -d name=random \
    -d artifactId=random \
    | tar zxf -

cp RandomApplication.java src/main/java/if4030/rest/

./mvnw spring-boot:run
